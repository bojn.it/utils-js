/**
 * Suggest standard `V`alue(s) of common `T`ype(s) to a programmer.
 *
 * @rel https://github.com/microsoft/TypeScript/issues/29729#issuecomment-505826972
 */
type Suggest<V extends T, T extends number | string = string> = V | (T & {})
